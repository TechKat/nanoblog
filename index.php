<?php
	
	/*
	*/
	
	
	/*
		You don't need to modify the code below, however if you feel you know what you're doing, you may edit the code to your needs.
		It is best to create a copy of the NanoBlog code *BEFORE* proceeding to edit any of the code, in case you run into some issues which you cannot undo.
	*/
	
	class NanoBlog {
		private static $_instance = null;
		private $_pdo,
				$_query,
				$_error = false,
				$_results,
				$_count = 0;
		
		private function __construct() {
			try {
				$this->_pdo = new PDO("mysql:host=hostnameHere;dbname=databaseHere", "userName", "passWord");
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}
		public static function dbGetInstance() {
			if(!isset(self::$_instance)) {
				self::$_instance = new NanoBlog();
			}
			return self::$_instance;
		}
		public function dbQuery($sql, $par = array()) {
			$this->_error = false;
			if($this->_query = $this->_pdo->prepare($sql)) {
				if(count($par)) {
					$x = 1;
					foreach($par as $p) {
						$this->_query->bindValue($x, $p);
						$x++;
					}
				}
				if($this->_query->execute()) {
					$this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
					$this->_count   = $this->_query->rowCount();
				} else {
					$this->_error = true;
				}
			}
			return $this;
		}
		public function dbAction($action, $table, $where = array(), $order = array()) {
			$ord = null;
			if(count($where) === 3) {
				$operators = array("=", ">", "<", ">=", "<=");
				$field     = $where[0];
				$operator  = $where[1];
				$value     = $where[2];
				if(count($order) === 2) {
					$ord .= " ORDER BY";
					$ord .= " `" . $order[0] . "`";
					$ord .= " " . $order[1];
				}
				if(in_array($operator, $operators)) {
					$sql = "{$action} FROM `{$table}` WHERE `{$field}` {$operator} ?{$ord}";
					if(!$this->query($sql, array($value))->error()) {
						return $this;
					}
				}
			} elseif((count($where) === 0) && (count($order) === 2)) {
				$sql = "{$action} FROM `{$table}` ORDER BY `" . $order[0] . "` " . $order[1];
				if(!$this->query($sql)->error()) {
					return $this;
				}
			} elseif(count($where) === 0) {
				$sql = "{$action} FROM `{$table}`";
				if(!$this->query($sql)->error()) {
					return $this;
				}
			} else {
				return false;
			}
		}
		public function dbGet($table, $where = array(), $order = array()) {
			return $this->action("SELECT *", $table, $where, $order);
		}
		public function dbInsert($table, $fields = array()) {
			if(count($fields)) {
				$keys = array_keys($fields);
				$vals = null;
				$x    = 1;
				foreach($fields as $f) {
					$vals .= "?";
					if($x < count($fields)) {
						$vals .= ", ";
					}
					$x++;
				}
				$sql  = "INSERT INTO `{$table}` (`" . implode("`, `", $keys) . "`) VALUES ({$vals});";
				if(!$this->query($sql, $fields)->error()) {
					return true;
				}
			}
			return false;
		}
		public function dbUpdate($table, $where, $fields = array()) {
			$set = null;
			$x   = 1;
			foreach($fields as $n => $v) {
				$set .= "{$n} = ?";
				if($x < count($fields)) {
					$set .= ", ";
				}
				$x++;
			}
			$sql = "UPDATE `{$table}` SET {$set} WHERE {$where}";
			if(!$this->query($sql, $fields)->error()) {
				return true;
			}
			return false;
		}
		public function dbDelete($table, $where = array()) {
			return $this->action("DELETE", $table, $where);
		}
		public function dbResults() {
			return $this->_results;
		}
		public function dbFirst() {
			return $this->results()[0];
		}
		public function dbError() {
			return $this->_error;
		}
		public function countResults() {
			return $this->_count;
		}
	}